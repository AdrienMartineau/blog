<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body/>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2019 Copyright
            <div class="right">
                <a class="grey-text text-lighten-4" href="https://www.linkedin.com/in/adrien-martineau/"> Adrien </a>
                <a class="grey-text text-lighten-4"> - </a>
                <a class="grey-text text-lighten-4" href="https://www.linkedin.com/in/angeline-toussaint/"> Angeline </a>
                <a class="grey-text text-lighten-4"> - </a>
                <a class="grey-text text-lighten-4" href="https://www.linkedin.com/in/manon-morille-b5aa1214b/"> Manon </a>
                <a class="grey-text text-lighten-4"> - </a>
                <a class="grey-text text-lighten-4" href="https://www.linkedin.com/in/marc-deshayes-a0b052145/"> Marc </a>
            </div>
        </div>
    </div>
</footer>

<body>

</body>
</html>
