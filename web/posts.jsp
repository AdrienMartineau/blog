<%@ page import="blog.beans.Blog" %>
<%@ page import="java.util.List" %>
<%@ page import="blog.dao.BlogDao" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% session.setAttribute("titleCss", "index"); %>

<h1><center>Liste des articles</center></h1>

<div class="row center">
    <div class="col s12 m4 offset-m4">
    <%
        List<Blog> list = new BlogDao().getBlogs();
        for (Blog post : list)
        {
            %>
            <div class="card">
                <div class="card-image">
                    <img src="img/img1.jpg">
                    <span class="card-title"><%= post.getTitre() %></span>
                </div>
                <div class="card-content">
                    <p><%= post.getDescription() %></p>
                </div>
                <div class="card-action">
                    <a href="article?id=<%= post.getId() %>">Lire l'article</a>
                </div>
            </div>
            <%
        }
    %>
    </div>
</div>