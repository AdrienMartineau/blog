<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Import css files-->
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/<%= session.getAttribute("titleCss") %>.css">

    <!--Import Jquery-->
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title><%= session.getAttribute("titlePage") %></title>
</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="./" class="brand-logo center">Blog</a>
        <ul class="left hide-on-med-and-down">
            <li class="active"><a href="creation-article">Créer un article</a></li>
            <li class="active"><a href="utilisateurs">Utilisateurs</a></li>
        </ul>
    </div>
</nav>
</body>