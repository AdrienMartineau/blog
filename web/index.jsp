<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% session.setAttribute("titlePage", "Blog"); %>
<% session.setAttribute("titleCss", "index"); %>
<jsp:include page="header.jsp" />

<% if (session.getAttribute("login") == null) {
    %>
    <div class="page">
    <div class="center-align btn-login-signin">
        <a class="btn waves-effect white grey-text text-darken-2 login">Connexion</a>
        <a class="btn waves-effect white grey-text text-darken-2 signin">Inscription</a>
    </div>

    <div class="carousel carousel-slider center" style="height: calc(100vh - 114px)">
        <div id="login" class="carousel-item">
            <div class="row">
                <form class="col s12 l4 offset-l4 card valign hoverable" method="post" action="connexion">
                    <div class="card-content">
                        <span class="card-title center-align">Connexion</span>
                        <p>${error}</p>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input id="loginEmail" name="email" type="email" required class="validate grey-text text-darken-3">
                                <label for="loginEmail">Email</label>
                            </div>
                            <div class="input-field col s12">
                                <i class="material-icons prefix">lock_outline</i>
                                <input id="loginPassword" name="password" type="password" required class="validate grey-text text-darken-3">
                                <label for="loginPassword">Mot de passe</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light teal">Se connecter
                                <i class="material-icons right">exit_to_app</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="signin" class="carousel-item">
            <div class="row">
                <form class="col s12 l4 offset-l4 card valign hoverable" method="post" action="inscription">
                    <div class="card-content">
                        <span class="card-title center-align">Inscription</span>
                        <p>${errorSignin}</p>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">email</i>
                                <input id="signinEmail" name="email" type="email" required class="validate grey-text text-darken-3">
                                <label for="signinEmail">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="signinNom" name="nom" type="text" required class="validate grey-text text-darken-3">
                                <label for="signinNom">Nom</label>
                            </div>
                            <div class="input-field col s5">
                                <i class="material-icons prefix">lock_outline</i>
                                <input id="signinPassword" name="password" type="password" required class="validate grey-text text-darken-3">
                                <label for="signinPassword">Mot de passe</label>
                            </div>
                            <div class="input-field col s7">
                                <input id="signinConfirmPassword" name="confirmPassword" type="password" required class="validate grey-text text-darken-3">
                                <label for="signinConfirmPassword">Confirmation du mot de passe</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light teal">S'inscrire
                                <i class="material-icons right">person_add</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <%
        } else {
    %>
    <jsp:include page="posts.jsp" />
    <%
}
%>



<jsp:include page= "footer.jsp" />

<script>
    $(document).ready(function () {
        // Init carousel
        $('.carousel').carousel({
            indicators: true,
            onCycleTo: function (slide) {
                if ($(slide).is('#login')) {
                    $('.btn.login').hide();
                    $('.btn.signin').show();
                } else {
                    $('.btn.signin').hide();
                    $('.btn.login').show();
                }
            }
        });

        // Show login when the user click on the login button
        $('body').on('click', '.login', function () {
            $('.carousel').carousel('set', 0);
        })

        // Show login when the user click on the login button
        $('body').on('click', '.signin', function () {
            $('.carousel').carousel('set', 1);
        })
    })
</script>