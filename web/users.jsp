<%@ page import="java.util.List" %>
<%@ page import="blog.beans.Utilisateur" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% session.setAttribute("titlePage", "Utilisateurs"); %>
<% session.setAttribute("titleCss", "users"); %>
<jsp:include page="header.jsp" />
<h1>Liste des utilisateurs</h1>

<table>
    <thead>
    <tr>
        <th>Email</th>
        <th>Nom</th>
        <th>Date d'inscription</th>
        <th>Administrateur</th>
    </tr>
    </thead>

    <tbody>
    <%
        List<Utilisateur> users = (List<Utilisateur>) request.getAttribute("users");
        for (Utilisateur user : users) {
            %>
            <tr>
                <td><%= user.getEmail() %></td>
                <td><%= user.getNom() %></td>
                <td><%= user.getDateCreation() %></td>
                <td><%= user.getAdmin() %></td>
            </tr>
            <%
        }
    %>
    </tbody>
</table>

<jsp:include page= "footer.jsp" />