<%@ page import="blog.beans.Blog" %>
<%@ page import="java.util.List" %>
<%@ page import="blog.beans.Reponse" %>
<%@ page import="blog.beans.Utilisateur" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% session.setAttribute("titlePage", "Articles"); %>
<% session.setAttribute("titleCss", "post"); %>
<jsp:include page="header.jsp" />
<%
    Blog post = (Blog) request.getAttribute("post");
%>

<page>
    <div class="row">
        <div class="col s12 m8 offset-m2">
            <div class="row" style="margin-top: 20px;">
                <a href="./" class="col s12">
                    <button class="btn waves-effect waves-light teal">
                        Retour <i class="material-icons left">arrow_back</i>
                    </button>
                </a>
            </div>

            <h3 class="header">
                <%= post.getTitre() %>
                <%
                    boolean is_admin = (boolean) session.getAttribute("is_admin");
                    if (is_admin) {
                        if (!post.getStatut().getId().equals(4)) {
                            %>
                            <a class="material-icons tooltipped modal-trigger" data-target="editPost"
                               data-position="bottom" data-tooltip="Modifier l'article" style="cursor: pointer">edit</a>
                            <%
                        }
                        %>
                        <a class="material-icons tooltipped" href="suppression-article?id=${id}"
                           data-position="bottom" data-tooltip="Supprimer l'article" style="cursor: pointer">delete</a>
                        <%
                    }
                %>
            </h3>
            <div class="card horizontal">
                <div class="card-image">
                    <img src="img/background.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p><%= post.getDescription() %></p>
                    </div>
                    <div class="card-action">
                        <a href="#">
                            <%= post.getCreateur().getNom() %> -
                            <i><%= post.getCreateur().getEmail() %></i> -
                            <%= post.getDateCreation() %> -
                            <%= post.getStatut().getTitle() %>
                        </a>
                    </div>
                </div>
            </div>

            <form class="row" method="post" action="ajout-commentaire?blogId=${id}" style="margin-bottom: 10px">
                <div class="input-field col s12 m8">
                    <input id="comment" name="comment" type="text" required class="validate grey-text text-darken-3">
                    <label for="comment">Votre commentaire...</label>
                </div>
                <button class="col s12 m4 btn waves-effect waves-light teal" style="margin-top: 25px">
                    Commenter <i class="material-icons right">message</i>
                </button>
            </form>
            <ul class="collection with-header">
                <li class="collection-header"><h4>Commentaires</h4></li>
                <%
                    for (Reponse c: post.getListOfReponses()) {
                %>
                <li class="collection-item avatar">
                    <i class="material-icons circle">person</i>
                    <span class="title"><%= c.getBlogger().getNom() %> - Publié le <%= c.getPublication() %></span>
                    <p><%= c.getCommentaire() %></p>
                    <%
                        Utilisateur u = (Utilisateur) session.getAttribute("user");
                        if (u.getEmail().equals( c.getBlogger().getEmail() )) {
                    %>
                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                    <%
                        }
                    %>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</page>

<!-- Modal Edit Post -->
<form id="editPost" class="modal" method="post" action="article?id=${id}">
    <div class="modal-content">
        <h4>Modification de l'article</h4>
        <div class="row">
            <div class="input-field col s12">
                <input id="title" name="title" type="text" class="validate grey-text text-darken-3" value="<%= post.getTitre() %>">
                <label for="title">Titre</label>
            </div>
            <div class="input-field col s12">
                <textarea id="description" name="description" class="materialize-textarea"><%= post.getDescription() %></textarea>
                <label for="description">Description</label>
            </div>
            <div class="input-field col s12">
                <select name="statut">
                    <option value="1">Temporaire</option>
                    <option value="2" selected>Publié</option>
                    <option value="3">Archivé</option>
                    <option value="4">Annulé</option>
                </select>
                <label>Statut</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="modal-close waves-effect waves-green btn-flat">Enregistrer</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.tooltipped').tooltip();

        $('select').formSelect();

        $('.modal').modal();
    })
</script>

<jsp:include page= "footer.jsp" />