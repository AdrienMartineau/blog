<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% session.setAttribute("titlePage", "Blog"); %>
<% session.setAttribute("titleCss", "index"); %>
<jsp:include page="header.jsp" />

<div class="page">
    <div class="row">
        <form class="col s12 l4 offset-l4 card valign hoverable" method="post" action="creation-article">
            <div class="card-content">
                <span class="card-title center-align">Créer un article</span>
                <p>${error}</p>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">title</i>
                        <input id="title" name="title" type="text" required class="validate grey-text text-darken-3">
                        <label for="title">Titre</label>
                    </div>
                    <div class="input-field col s12">
                        <i class="material-icons prefix">description</i>
                        <textarea id="description" name="description" required class="validate materialize-textarea grey-text text-darken-3"></textarea>
                        <label for="description">Description</label>
                    </div>
                </div>
                <div class="row center-align">
                    <button class="btn waves-effect waves-light teal">Valider
                        <i class="material-icons right">done</i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page= "footer.jsp" />