package blog.dao;

import blog.beans.Utilisateur;

import java.sql.SQLException;
import java.util.List;

public interface IUtilisateurDao {

	Utilisateur getUtilisateur(String email);
	List<Utilisateur> getUtilisateurs();
	void createUtilisateur(Utilisateur utilisateur);
	void updateUtilisateur(Utilisateur utilisateur);
	void deleteUtilisateur(Utilisateur utilisateur);
	
}
