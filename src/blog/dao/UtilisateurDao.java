package blog.dao;

import blog.beans.Statut;
import blog.beans.Utilisateur;
import blog.servlets.LoginServlet;
import blog.utils.DateConversion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.acl.LastOwnerException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UtilisateurDao implements IUtilisateurDao {

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    private static final String QUERY_GET_ALL = "SELECT * FROM USERS";
    private static final String QUERY_GET_BY_EMAIL = QUERY_GET_ALL + " WHERE email = ?";
    private static final String QUERY_INSERT = "INSERT INTO USERS VALUES (?, ?, ?, ?, ?)";

    @Override
    public Utilisateur getUtilisateur(String email) {
        Utilisateur u = new Utilisateur();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_BY_EMAIL, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_BY_EMAIL);

            st.setString( 1, email );

            ResultSet rs = st.executeQuery();

            if ( rs.next() ) {
                u.setAdmin( rs.getBoolean("is_admin") );
                u.setNom( rs.getString("nom") );
                u.setEmail( rs.getString("email") );
                Date date = DateConversion.convertStringToDate(rs.getString("date_creation"));
                u.setDateCreation( DateConversion.convertUtilToSql(date) );
                u.setPassord( rs.getString("password") );
            }

        } catch (SQLException | ParseException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }
        return u;
    }

    @Override
    public List<Utilisateur> getUtilisateurs() {
        List<Utilisateur> users = new ArrayList<>();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_ALL, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_ALL);

            ResultSet rs = st.executeQuery();

            while ( rs.next() ) {
                Utilisateur u = new Utilisateur();
                u.setAdmin( rs.getBoolean("is_admin") );
                u.setNom( rs.getString("nom") );
                u.setEmail( rs.getString("email") );
                Date date = DateConversion.convertStringToDate(rs.getString("date_creation"));
                u.setDateCreation( DateConversion.convertUtilToSql(date) );
                u.setPassord( rs.getString("password") );

                users.add(u);
            }

        } catch (SQLException | ParseException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return users;
    }

    @Override
    public void createUtilisateur(Utilisateur utilisateur) {
        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_INSERT, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_INSERT);

            st.setString( 1, utilisateur.getEmail() );
            st.setString( 2, utilisateur.getNom() );
            st.setDate( 3, utilisateur.getDateCreation() );
            st.setString( 4, utilisateur.getPassord() );
            st.setBoolean( 5, utilisateur.getAdmin() );

            st.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }
    }

    @Override
    public void updateUtilisateur(Utilisateur utilisateur) {

    }

    @Override
    public void deleteUtilisateur(Utilisateur utilisateur) {

    }
}
