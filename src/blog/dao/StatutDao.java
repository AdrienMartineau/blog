package blog.dao;

import blog.beans.Blog;
import blog.beans.Reponse;
import blog.beans.Statut;
import blog.beans.Utilisateur;
import blog.servlets.LoginServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StatutDao implements IStatutDao {

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    private static final String QUERY_GET_ALL = "SELECT * FROM STATUT";
    private static final String QUERY_GET_BY_ID = QUERY_GET_ALL + " WHERE id = ?";

    @Override
    public Statut getStatut(Integer id) {
        Statut s = new Statut();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_BY_ID, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_BY_ID);

            st.setInt(1, id);

            ResultSet rs = st.executeQuery();

            if ( rs.next() ) {
                s.setId( Integer.parseInt(rs.getString("id")) );
                s.setTitle( rs.getString("title") );
            }

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return s;
    }

    @Override
    public List<Statut> getListOfStatuts() {
        List<Statut> statuts = new ArrayList<Statut>();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_ALL, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_ALL);

            ResultSet rs = st.executeQuery();

            while ( rs.next() ) {
                Statut s = new Statut();
                s.setId( Integer.parseInt(rs.getString("id")) );
                s.setTitle( rs.getString("title") );
                statuts.add(s);
            }

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return statuts;
    }
}
