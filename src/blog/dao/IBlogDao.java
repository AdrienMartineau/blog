package blog.dao;

import blog.beans.Blog;
import blog.beans.Reponse;

import java.sql.SQLException;
import java.util.List;

public interface IBlogDao {

	Blog getBlog(Integer id);
	List<Blog> getBlogs();
	List<Reponse> getCommentsById(Integer id);
	Integer createBlog(Blog blog) throws SQLException;
	void updateBlog(Blog blog) throws SQLException;
	void deleteBlog(Blog blog) throws SQLException;
	void addReponse(Blog blog, Reponse reponse) throws SQLException;
	
}
