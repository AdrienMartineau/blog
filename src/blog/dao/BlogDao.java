package blog.dao;

import blog.beans.Blog;
import blog.beans.Reponse;
import blog.servlets.LoginServlet;
import blog.utils.DateConversion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlogDao implements IBlogDao {

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    private static final String QUERY_GET_ALL = "SELECT * FROM BLOG";
    private static final String QUERY_GET_BY_ID = QUERY_GET_ALL + " WHERE ID = ?";
    private static final String QUERY_GET_COMMENTS_BY_ID = "SELECT * FROM BLOG_COMMENTAIRES WHERE BLOG_ID = ?";
    private static final String QUERY_INSERT_COMMENT = "INSERT INTO BLOG_COMMENTAIRES(COMMENTAIRE, EMAIL, DATE_CREATION, BLOG_ID) VALUES (?, ?, ?, ?)";
    private static final String QUERY_UPDATE = "UPDATE BLOG SET TITRE = ?, DESCRIPTION = ?, STATUT = ? WHERE ID = ?";
    private static final String QUERY_DELETE = "DELETE FROM BLOG WHERE ID = ?";
    private static final String QUERY_INSERT = "INSERT INTO BLOG(ID, TITRE, DESCRIPTION, EMAIL, DATE_CREATION, STATUT) VALUES (?, ?, ?, ?, ?, ?)";

    @Override
    public Blog getBlog(Integer id) {

        Blog b = new Blog();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_BY_ID, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_BY_ID);

            st.setInt( 1, id );

            ResultSet rs = st.executeQuery();

            if ( rs.next() ) {
                b.setId(id);
                b.setTitre( rs.getString("titre") );
                b.setDescription( rs.getString("description") );
                b.setCreateur( new UtilisateurDao().getUtilisateur( rs.getString("email") ) );
                b.setDateCreation(
                    DateConversion.convertUtilToSql( DateConversion.convertStringToDate(rs.getString("date_creation")) )
                );
                String dateModification = rs.getString("date_modification");
                if (dateModification != null) {
                    b.setDateModification(
                        DateConversion.convertUtilToSql( DateConversion.convertStringToDate(dateModification) )
                    );
                }
                b.setStatut( new StatutDao().getStatut(rs.getInt("statut")) );
                b.setListOfReponses( this.getCommentsById(id) );
            }

        } catch (SQLException | ParseException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return b;
    }

    @Override
    public List<Blog> getBlogs() {
        List<Blog> posts = new ArrayList<>();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_ALL, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_ALL);

            ResultSet rs = st.executeQuery();

            while ( rs.next() ) {
                Blog b = new Blog();
                b.setId( rs.getInt("id") );
                b.setTitre( rs.getString("titre") );
                b.setDescription( rs.getString("description") );
                b.setCreateur( new UtilisateurDao().getUtilisateur( rs.getString("email") ) );
                b.setDateCreation(
                    DateConversion.convertUtilToSql( DateConversion.convertStringToDate(rs.getString("date_creation")) )
                );
                String dateModification = rs.getString("date_modification");
                if (dateModification != null) {
                    b.setDateModification(
                            DateConversion.convertUtilToSql( DateConversion.convertStringToDate(dateModification) )
                    );
                }
                b.setStatut( new StatutDao().getStatut(rs.getInt("statut")) );

                posts.add(b);
            }

        } catch (SQLException | ParseException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return posts;
    }

    @Override
    public List<Reponse> getCommentsById(Integer id) {
        List<Reponse> comments = new ArrayList<>();

        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_GET_COMMENTS_BY_ID, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_GET_COMMENTS_BY_ID);

            st.setInt( 1, id );

            ResultSet rs = st.executeQuery();

            while ( rs.next() ) {
                Reponse r = new Reponse();
                r.setCommentaire( rs.getString("commentaire") );
                r.setBlogger( new UtilisateurDao().getUtilisateur( rs.getString("email") ) );
                r.setPublication(
                    DateConversion.convertUtilToSql( DateConversion.convertStringToDate(rs.getString("date_creation")) )
                );

                comments.add(r);
            }

        } catch (SQLException | ParseException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return comments;
    }

    @Override
    public Integer createBlog(Blog blog) {
        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_INSERT, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_INSERT);

            st.setInt( 1, blog.getId() );
            st.setString( 2, blog.getTitre() );
            st.setString( 3, blog.getDescription() );
            st.setString( 4, blog.getCreateur().getEmail() );
            st.setDate( 5, blog.getDateCreation() );
            st.setInt( 6, blog.getStatut().getId() );

            st.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }

        return blog.getId();
    }

    @Override
    public void updateBlog(Blog blog) {
        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_UPDATE, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_UPDATE);

            st.setString( 1, blog.getTitre() );
            st.setString( 2, blog.getDescription() );
            st.setInt( 3, blog.getStatut().getId() );
            st.setInt( 4, blog.getId() );

            st.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }
    }

    @Override
    public void deleteBlog(Blog blog) {
        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_DELETE, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_DELETE);

            st.setInt( 1, blog.getId() );

            st.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }
    }

    @Override
    public void addReponse(Blog blog, Reponse reponse) {
        try {
            Connection connection = PersistenceManager.getConnection();
            PreparedStatement st = connection.prepareStatement( QUERY_INSERT_COMMENT, Statement.RETURN_GENERATED_KEYS );

            logger.debug("Requête sql: " + QUERY_INSERT_COMMENT);

            st.setString( 1, reponse.getCommentaire() );
            st.setString( 2, reponse.getBlogger().getEmail() );
            st.setDate( 3, reponse.getPublication() );
            st.setInt( 4, blog.getId() );

            st.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getStackTrace());

            System.out.println(e.getStackTrace());
        }
    }
}
