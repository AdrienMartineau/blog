package blog.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PersistenceManager {
	
	private static final String DB_URL = "jdbc:hsqldb:hsql://localhost:9003";
	private static final String DB_LOGIN = "SA";
	private static final String DB_PWD = "";
	
	private static Connection connection;
	
	private PersistenceManager() {}
	
	public static Connection getConnection() throws SQLException {
		if ( null == connection || connection.isClosed() ) {
			try {
				Class.forName("org.hsqldb.jdbcDriver").newInstance();
			} catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
				System.out.println(e.getStackTrace());
			}

			connection = DriverManager.getConnection( DB_URL, DB_LOGIN, DB_PWD );
		}

		return connection;
	}
	
	public static void closeConnection() throws SQLException {
		if ( null != connection && !connection.isClosed() ) {
			connection.close();
		}
	}
}
