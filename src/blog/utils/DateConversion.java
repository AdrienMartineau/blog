package blog.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateConversion {

    public static java.util.Date convertStringToDate(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
        return dateFormat.parse(date);
    }

    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        return new java.sql.Date(uDate.getTime());
    }

}
