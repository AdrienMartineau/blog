package blog.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Number {

    public static boolean isNumber(String number) {
        try {
            int n = Integer.parseInt(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
