package blog.servlets;

import blog.beans.Blog;
import blog.dao.BlogDao;
import blog.dao.StatutDao;
import blog.utils.Number;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/suppression-article")
public class RemovePostServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(RemovePostServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET /article");

        String id = request.getParameter("id");

        if (!Number.isNumber(id) || id == null) {
            response.sendRedirect("./");
        } else {
            Blog b = new BlogDao().getBlog( Integer.parseInt(id) );

            if (b.getId() != null) {
                new BlogDao().deleteBlog(b);
            }

            response.sendRedirect("./");
        }
    }
}
