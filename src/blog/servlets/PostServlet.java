package blog.servlets;

import blog.beans.Blog;
import blog.beans.Utilisateur;
import blog.dao.BlogDao;
import blog.dao.StatutDao;
import blog.dao.UtilisateurDao;
import blog.utils.Number;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/article")
public class PostServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(PostServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET /article");

        String id = request.getParameter("id");

        if (!Number.isNumber(id) || id == null) {
            response.sendRedirect("./");
        } else {
            Blog b = new BlogDao().getBlog( Integer.parseInt(id) );

            if (b.getId() == null) {
                response.sendRedirect("./");
            } else {
                request.setAttribute("id", id);
                request.setAttribute("post", b);

                request.getRequestDispatcher("/post.jsp").forward(request, response);
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("POST /article");

        String id = request.getParameter("id");

        if (!Number.isNumber(id) || id == null) {
            response.sendRedirect("./");
        } else {
            Blog b = new BlogDao().getBlog( Integer.parseInt(id) );

            b.setTitre( request.getParameter("title") );
            b.setDescription( request.getParameter("description") );

            String statut = request.getParameter("statut");
            if (!statut.isEmpty() && Number.isNumber(statut)) {
                b.setStatut( new StatutDao().getStatut( Integer.parseInt(statut) ));
            }

            new BlogDao().updateBlog(b);

            response.sendRedirect("article?id=" + id);
        }

    }
}
