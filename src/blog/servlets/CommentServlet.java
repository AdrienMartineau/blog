package blog.servlets;

import blog.beans.Blog;
import blog.beans.Reponse;
import blog.beans.Utilisateur;
import blog.dao.BlogDao;
import blog.dao.UtilisateurDao;
import blog.utils.DateConversion;
import blog.utils.Number;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet("/ajout-commentaire")
public class CommentServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(CommentServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("POST /ajout-commentaire");

        String id = request.getParameter("blogId");

        // Check is number or null
        if (!Number.isNumber(id) || id == null) {
            response.sendRedirect("./");
        } else {
            String comment = request.getParameter("comment");

            if (!comment.isEmpty()) {
                HttpSession session = request.getSession();

                Blog b = new BlogDao().getBlog( Integer.parseInt(id) );

                if (b.getId() == null) {
                    response.sendRedirect("./");
                } else {
                    Reponse r = new Reponse();
                    r.setBlog(b);
                    r.setBlogger( (Utilisateur) session.getAttribute("user") );
                    r.setCommentaire(comment);
                    r.setPublication( DateConversion.convertUtilToSql( new Date() ) );
                    new BlogDao().addReponse(b, r);

                    response.sendRedirect("./article?id=" + id);
                }

            } else {
                logger.error("Aucun commentaire n'a été indiqué");

                response.getWriter().append("Aucun commentaire n'a été indiqué").append(request.getContextPath());
            }
        }
    }

}
