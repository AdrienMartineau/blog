package blog.servlets;

import blog.beans.Blog;
import blog.beans.Utilisateur;
import blog.dao.BlogDao;
import blog.dao.StatutDao;
import blog.dao.UtilisateurDao;
import blog.utils.DateConversion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/creation-article")
public class CreatePostServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET /creation-article");

        request.getRequestDispatcher("/createpost.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("POST /creation-article");

        String title = request.getParameter("title");
        String description = request.getParameter("description");

        List<Blog> posts = new BlogDao().getBlogs();
        int lastBlogId = posts.get( posts.size() - 1 ).getId();

        Blog b = new Blog();
        b.setId( lastBlogId + 1 );
        b.setTitre( title );
        b.setDescription( description );

        HttpSession session = request.getSession();
        b.setCreateur( (Utilisateur) session.getAttribute("user") );
        b.setStatut( new StatutDao().getStatut(2) );
        b.setDateCreation(
            DateConversion.convertUtilToSql( new Date() )
        );

        new BlogDao().createBlog(b);

        response.sendRedirect("article?id=" + b.getId());
    }
}
