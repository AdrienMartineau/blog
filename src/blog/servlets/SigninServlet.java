package blog.servlets;

import blog.beans.Utilisateur;
import blog.dao.UtilisateurDao;
import blog.utils.DateConversion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

@WebServlet("/inscription")
public class SigninServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(SigninServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET /inscription");

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("POST /inscription");

        String pw = request.getParameter("password");
        String confirmpw = request.getParameter("confirmPassword");
        if (pw.equals(confirmpw)) {
            Utilisateur u = new Utilisateur();
            u.setNom( request.getParameter("nom") );
            u.setEmail( request.getParameter("email") );
            u.setPassord(pw);
            u.setDateCreation( DateConversion.convertUtilToSql(new java.util.Date()) );
            u.setAdmin(false);

            new UtilisateurDao().createUtilisateur(u);
        } else {
            request.setAttribute("errorSignin", "Les mots de passe doivent être identique.");
        }
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
