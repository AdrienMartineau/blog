package blog.servlets;

import blog.beans.Utilisateur;
import blog.dao.UtilisateurDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/connexion")
public class LoginServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET /connexion");

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("POST /connexion");

        String email = request.getParameter("email");
        String pw = request.getParameter("password");

        Utilisateur u = new UtilisateurDao().getUtilisateur(email);

        if (u.getEmail() != null && u.getPassord().equals(pw)) {
            HttpSession session = request.getSession();
            session.setAttribute("login", true);
            session.setAttribute("is_admin", u.getAdmin());
            session.setAttribute("user", u);
        } else {
            logger.error("Informations non valides : " + email);
            
            request.setAttribute("error", "Informations non valides");
        }

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

}
